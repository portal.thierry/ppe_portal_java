package Util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Singleton {
	
	public Connection cnx = null;
	private static Singleton instance = null;
	private static String dsn = null;
	private static String username = null;
	private static String password = null;
	
	private Singleton() throws SQLException {
		
		dsn = "jdbc:mariadb://127.0.0.1:3306/pharmacie";
		username = "pharmacieuser";
		password = "pharmacie2023";
		
		cnx = DriverManager.getConnection(dsn, username, password);

	}
	
	public static Singleton getInstance() throws SQLException {
		
		if (instance ==  null)
        {
            instance = new Singleton();
            System.out.println("Connexion !");
            
        }
		return instance;
	}
	
}
