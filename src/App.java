import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;

import Controller.LoginController;
import DAO.DAOUser;
import Util.HibernateSessionFactory;
import Views.FenetreLogin;


public class App {
	
	public static void testConnexion() {
		
		try {
			DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3306/pharmacie", "pharmacieuser", "pharmacie2023");
			System.out.println("Connecté");
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {	
		Session session = HibernateSessionFactory.getSessionFactory().openSession();
		
		//Creation fenetres
		FenetreLogin fLog = new FenetreLogin();
		
		//Creation DAO User
		DAOUser daou = new DAOUser(session);
		
		//Creation controllers
		new LoginController(fLog,daou,session).init();
	}

}
