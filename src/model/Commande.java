package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the COMMANDE database table.
 * 
 */
@Entity
@Table(name="COMMANDE")
@NamedQuery(name="Commande.findAll", query="SELECT c FROM Commande c")
public class Commande implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private int nbLot;

	//bi-directional many-to-one association to Lot
	@ManyToOne
	@JoinColumn(name="idLot")
	private Lot lot;

	//bi-directional many-to-one association to Pharmacie
	@ManyToOne
	@JoinColumn(name="idPharmacie")
	private Pharmacie pharmacie;

	public Commande() {
	}

	public Commande(int id, int nbLot, Lot lot, Pharmacie pharmacie) {
		super();
		this.id = id;
		this.nbLot = nbLot;
		this.lot = lot;
		this.pharmacie = pharmacie;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNbLot() {
		return this.nbLot;
	}

	public void setNbLot(int nbLot) {
		this.nbLot = nbLot;
	}

	public Lot getLot() {
		return this.lot;
	}

	public void setLot(Lot lot) {
		this.lot = lot;
	}

	public Pharmacie getPharmacie() {
		return this.pharmacie;
	}

	public void setPharmacie(Pharmacie pharmacie) {
		this.pharmacie = pharmacie;
	}

}