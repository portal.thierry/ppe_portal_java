package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the PHARMACIE database table.
 * 
 */
@Entity
@Table(name="PHARMACIE")
@NamedQuery(name="Pharmacie.findAll", query="SELECT p FROM Pharmacie p")
public class Pharmacie implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String adresse;

	private String nomEntreprise;

	private String telephone;

	//bi-directional many-to-one association to Commande
	@OneToMany(mappedBy="pharmacie")
	private List<Commande> commandes;

	public Pharmacie() {
	}

	public Pharmacie(int id, String adresse, String nomEntreprise, String telephone, List<Commande> commandes) {
		super();
		this.id = id;
		this.adresse = adresse;
		this.nomEntreprise = nomEntreprise;
		this.telephone = telephone;
		this.commandes = commandes;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getNomEntreprise() {
		return this.nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getTelephone() {
		return this.telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public List<Commande> getCommandes() {
		return this.commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Commande addCommande(Commande commande) {
		getCommandes().add(commande);
		commande.setPharmacie(this);

		return commande;
	}

	public Commande removeCommande(Commande commande) {
		getCommandes().remove(commande);
		commande.setPharmacie(null);

		return commande;
	}

}