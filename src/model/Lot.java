package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the LOT database table.
 * 
 */
@Entity
@Table(name="LOT")
@NamedQuery(name="Lot.findAll", query="SELECT l FROM Lot l")
public class Lot implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int num;

	private int quantite;

	//bi-directional many-to-one association to Commande
	@OneToMany(mappedBy="lot")
	private List<Commande> commandes;

	//bi-directional many-to-one association to Medicament
	@ManyToOne
	@JoinColumn(name="typeMedicament")
	private Medicament medicament;

	//bi-directional many-to-one association to Usine
	@ManyToOne
	@JoinColumn(name="usineDeProduction")
	private Usine usine1;

	public Lot() {
	}

	public int getNum() {
		return this.num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getQuantite() {
		return this.quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public List<Commande> getCommandes() {
		return this.commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

	public Commande addCommande(Commande commande) {
		getCommandes().add(commande);
		commande.setLot(this);

		return commande;
	}

	public Commande removeCommande(Commande commande) {
		getCommandes().remove(commande);
		commande.setLot(null);

		return commande;
	}

	public Medicament getMedicament() {
		return this.medicament;
	}

	public void setMedicament(Medicament medicament) {
		this.medicament = medicament;
	}

	public Usine getUsine1() {
		return this.usine1;
	}

	public void setUsine1(Usine usine1) {
		this.usine1 = usine1;
	}

}