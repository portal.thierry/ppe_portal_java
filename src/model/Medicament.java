package model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the MEDICAMENT database table.
 * 
 */
@Entity
@Table(name="MEDICAMENT")
@NamedQuery(name="Medicament.findAll", query="SELECT m FROM Medicament m")
public class Medicament implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="Nom")
	private String nom;

	@Column(name="Prix")
	private BigDecimal prix;

	//bi-directional many-to-one association to Lot
	@OneToMany(mappedBy="medicament")
	private List<Lot> lots;

	public Medicament() {
	}
	
	public Medicament(int id, String nom, BigDecimal prix) {
		super();
		this.id = id;
		this.nom = nom;
		this.prix = prix;
	}
	
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public BigDecimal getPrix() {
		return this.prix;
	}

	public void setPrix(BigDecimal prix) {
		this.prix = prix;
	}

	public List<Lot> getLots() {
		return this.lots;
	}

	public void setLots(List<Lot> lots) {
		this.lots = lots;
	}

	public Lot addLot(Lot lot) {
		getLots().add(lot);
		lot.setMedicament(this);

		return lot;
	}

	public Lot removeLot(Lot lot) {
		getLots().remove(lot);
		lot.setMedicament(null);

		return lot;
	}

}