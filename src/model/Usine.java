package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the USINE database table.
 * 
 */
@Entity
@Table(name="USINE")
@NamedQuery(name="Usine.findAll", query="SELECT u FROM Usine u")
public class Usine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String adresse;

	private String nom;

	private String ville;

	//bi-directional many-to-one association to Lot
	@OneToMany(mappedBy="usine1")
	private List<Lot> lots1;

	public Usine() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getVille() {
		return this.ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public List<Lot> getLots1() {
		return this.lots1;
	}

	public void setLots1(List<Lot> lots1) {
		this.lots1 = lots1;
	}

	public Lot addLots1(Lot lots1) {
		getLots1().add(lots1);
		lots1.setUsine1(this);

		return lots1;
	}

	public Lot removeLots1(Lot lots1) {
		getLots1().remove(lots1);
		lots1.setUsine1(null);

		return lots1;
	}

}