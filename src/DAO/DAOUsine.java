package DAO;

import org.hibernate.Session;

import model.Usine;

public class DAOUsine extends DAOGeneric<Usine>{

	public DAOUsine(Session session) {
		super(session, Usine.class);
	}
}
	
