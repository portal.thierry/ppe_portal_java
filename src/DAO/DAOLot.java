package DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import model.Lot;
import model.Medicament;

public class DAOLot extends DAOGeneric<Lot>{

	public DAOLot(Session session) {
		super(session, Lot.class);
		//TODO Auto-generated constructor stub
	}
	
	public List<Lot> getLotsById(int id)
	{
		String HQL = "FROM Lot WHERE typeMedicament = :id";
		Query query = session.createQuery(HQL);
		query.setParameter("id", id);
		
		List<Lot> lots = query.getResultList();
		
		return lots;
	}
	
	public List<Lot> getLotsByUsine(int id)
	{
		String HQL = "FROM Lot WHERE usineDeProduction = :id";
		Query query = session.createQuery(HQL);
		query.setParameter("id", id);
		
		List<Lot> lots = query.getResultList();
		
		return lots;
	}
	
	public int getLatestNumLot()
	{
		Query query = session.createQuery("FROM Lot as l order by num DESC");	
		query.setMaxResults(1);	
		Lot lot = (Lot) query.uniqueResult();	

		return lot.getNum();
	}

}
