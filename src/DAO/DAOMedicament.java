package DAO;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import model.Medicament;

public class DAOMedicament extends DAOGeneric<Medicament>{

	public DAOMedicament(Session session) {
		super(session, Medicament.class);
		//TODO Auto-generated constructor stub
	}
	
	public List<Medicament> findMedicamentsByName(String startingLetter)
	{
		String HQL = "FROM Medicament as m WHERE m.nom LIKE :startingLetter ";
		Query query = session.createQuery(HQL);
		query.setString("startingLetter", "%" + startingLetter + "%");
		
		List<Medicament> medicaments = query.getResultList();
		
		return medicaments;
	}

	public int getLatestIdMedicament()
	{
		Query query = session.createQuery("FROM Medicament as m order by id DESC");	
		query.setMaxResults(1);	
		Medicament medicament = (Medicament) query.uniqueResult();	

		return medicament.getId();
	}
}
