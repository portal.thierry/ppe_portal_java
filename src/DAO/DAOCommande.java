package DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import model.Commande;

public class DAOCommande extends DAOGeneric<Commande>{

	public DAOCommande(Session session) {
		super(session, Commande.class);
		//TODO Auto-generated constructor stub
	}
	
	public int getLatestIdCommande()
	{
		Query query = session.createQuery("FROM Commande as c order by id DESC");	
		query.setMaxResults(1);	
		Commande commande = (Commande) query.uniqueResult();	

		return commande.getId();
	}
	
	public List<Commande> getCommandesForPharmacie(int pharmacieConnectee)
	{
		String HQL = "FROM Commande as c WHERE idPharmacie = :pharmacieConnectee";
		Query query = session.createQuery(HQL);
		query.setParameter("pharmacieConnectee",pharmacieConnectee);
		List<Commande> commandes = query.getResultList();
		System.out.println(commandes);

		return commandes;
	}
}
