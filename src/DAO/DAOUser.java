package DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import model.Medicament;
import model.User;

public class DAOUser extends DAOGeneric<User>{

	public DAOUser(Session session) {
		super(session, User.class);
		//TODO Auto-generated constructor stub
	}

	public User findUserByLogin(String id)
	{
		String HQL = "FROM User as u WHERE u.username = :id ";
		Query query = session.createQuery(HQL);
		query.setString("id",id );
		
		query.setMaxResults(1);	
		User user = (User) query.uniqueResult();	
		
		return user;
	}
}
