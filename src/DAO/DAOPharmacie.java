package DAO;

import org.hibernate.Query;
import org.hibernate.Session;

import model.Pharmacie;

public class DAOPharmacie extends DAOGeneric<Pharmacie>{

	public DAOPharmacie(Session session) {
		super(session, Pharmacie.class);
		//TODO Auto-generated constructor stub
	}

	public int getLatestIdPharmacie()
	{
		Query query = session.createQuery("FROM Pharmacie as p order by id DESC");	
		query.setMaxResults(1);	
		Pharmacie pharmacie = (Pharmacie) query.uniqueResult();	

		return pharmacie.getId();
	}
	
}
