package Controller;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.Medicament;

public class MyDefaultTableModelMedicament extends DefaultTableModel{
	
	List<Medicament> medicaments;
	private String columnNames [] = {"Id", "Nom", "Prix"};
	HashSet<Medicament> cModif;

	public MyDefaultTableModelMedicament(List<Medicament> medicaments) {
		super();
		this.medicaments = medicaments;
		cModif = new HashSet<Medicament>();
	}

	@Override
	public int getRowCount() {
		if (medicaments == null) {
			return 0;
		} else {
		return medicaments.size();
		}
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
		
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column > 0);
	}

	@Override
	public Object getValueAt(int row, int column) {
	
		Medicament medicament = medicaments.get(row);
		Object value = null;
		
		switch (column) {
			case 0:
				value = medicament.getId();
				break;
			case 1:
				value = medicament.getNom();
				break;
			case 2:
				value = medicament.getPrix();
				break;

			default:
				break;
		}
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {		
		
		Medicament medicament = medicaments.get(row);
		Object value = null;
		
		switch (column) {

			case 1:
				medicament.setNom((String) aValue);
				break;
			case 2:
				medicament.setPrix((BigDecimal) aValue);
				break;
				
			default:
				break;		
		}
		
		cModif.add(medicament);
		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		
		switch (columnIndex) {
			case 0:
				type = Integer.class;
				break;		
			case 1 :
				type = String.class;
				break;
			case 2 :
				type = BigDecimal.class;
				break;
			
			default:
				break;
		}
		
		return type;
	}

	
}
