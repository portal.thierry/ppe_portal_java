package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Objects;

import javax.swing.JOptionPane;

import DAO.DAOCommande;
import DAO.DAOLot;
import DAO.DAOMedicament;
import DAO.DAOPharmacie;
import DAO.DAOUsine;
import Views.Fenetre01;
import Views.FenetreLot;
import model.Commande;
import model.Lot;
import model.Medicament;
import model.Pharmacie;
import model.User;
import model.Usine;

public class LotController {

	Fenetre01 fenetre01;
	FenetreLot fenetreLot;
	DAOLot daol;
	DAOMedicament daom;
	DAOUsine daou;
	DAOPharmacie daop;
	DAOCommande daoc;
	Lot lot;
	int idMedicament;
	List<Lot> lots;
	MyDefaultTableModelMedicament mDTMM;
	MyDefaultTableModelLot mDTML;
	private User user;
	
	public LotController(List<Lot> lots,Fenetre01 fenetre01,FenetreLot fenetreLot, DAOLot daol, DAOMedicament daom, DAOUsine daou, DAOPharmacie daop, DAOCommande daoc,User user, int idMedicament) {
		super();
		this.lots = lots;
		this.fenetre01 = fenetre01;
		this.fenetreLot = fenetreLot;
		this.daol = daol;
		this.daom = daom;
		this.daou = daou;
		this.daop = daop;
		this.daoc = daoc;
		this.user = user;
		this.idMedicament = idMedicament;
		
		fenetreLot.getBtnRetour().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doRetour();				
			}

		});	
		if ("usine".equals(user.getRoleBean().getLibelle())) {
			fenetreLot.getBtnAjouter().addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					doAdd(LotController.this.idMedicament);	
					fenetreLot.getTextFieldQtt().setText("");
					doRetour();
				}
	
			});	
		}
		if ("client".equals(user.getRoleBean().getLibelle())) {
			fenetreLot.getBtnAcheter().addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					doAcheter();
					doRetour();
					fenetreLot.getTextFieldQtt().setText(null);
				}
	
			});	
			fenetreLot.getTable().addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
			      if (e.getClickCount() == 2) {
			          //Code pour gerer le double click	  
			    	  String id =getIdCellSelected();
			    	  System.out.println(id);
			    	  
			        }
				}

			});	
		}
	}
	
	private void doRetour() {	
		fenetreLot.setVisible(false);
		fenetre01.setVisible(true);
	}
	
	private void doAdd(int idMedicament) {	
		boolean action = false;
		int i = 0;
		String qtt = fenetreLot.getTextFieldQtt().getText();
		Medicament medicament = daom.find(idMedicament);
		Usine usine = daou.find(user.getId());
		
		if (lots.isEmpty()) {
			int num = daol.getLatestNumLot();
			Lot newLot = new Lot();
			newLot.setNum(num+1);
			newLot.setQuantite(Integer.parseInt(qtt));
			newLot.setMedicament(medicament);
			newLot.setUsine1(usine);
			daol.save(newLot);
			action = true;
		}
		
		for (Lot lot : lots) {

			if (user.getId() == lot.getUsine1().getId() && action == false) {			
				int qttTotal = lot.getQuantite() + Integer.parseInt(qtt);
				lot.setQuantite(qttTotal);
				daol.update(lot);
				action = true;			
			}
			if (lots.size()-1 == i && action == false) {
				Lot newLot = new Lot();
				newLot.setNum(daol.getLatestNumLot()+1);
				newLot.setQuantite(Integer.parseInt(qtt));
				newLot.setMedicament(medicament);
				newLot.setUsine1(usine);
				daol.save(newLot);
				action = true;
			}
			i++;
		}
	}
	
	private void doAcheter() {	
		String qtt = fenetreLot.getTextFieldQtt().getText();
		boolean action = false;
		String nomUsine = getIdCellSelected();
		Commande commande = new Commande();
		Pharmacie pharmacie = new Pharmacie();
		
		for (Lot lot : lots) {
			if (Objects.equals(nomUsine, lot.getUsine1().getNom()) && action == false) {			
				int qttTotal = lot.getQuantite() - Integer.parseInt(qtt);
					if (qttTotal > 0) {
						
						pharmacie = daop.find(user.getId());
						int idCommande = daoc.getLatestIdCommande();
						lot.setQuantite(qttTotal);
						commande.setId(idCommande+1);
						commande.setLot(lot);
						commande.setNbLot(Integer.parseInt(qtt));
						commande.setPharmacie(pharmacie);
						daol.update(lot);
						daoc.save(commande);
						action = true;	
					}
					else {
						JOptionPane.showMessageDialog(null, "Vous achetez plus de lots qu'il n'y en a de disponible","Erreur", JOptionPane.ERROR_MESSAGE);
					}
			}
		}
	}
	
	private String getIdCellSelected() {			
		int rows = fenetreLot.getTable().getSelectedRow();
		int columns = fenetreLot.getTable().getSelectedColumn();
		String nomUsine = (String) fenetreLot.getTable().getValueAt(rows, columns);
		return nomUsine;
	}

	public int getIdMedicament() {
		return idMedicament;		
	}

	public void setIdMedicament(int idMedicament) {
		this.idMedicament = idMedicament;		
	}
	
}
