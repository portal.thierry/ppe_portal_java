package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import org.hibernate.Session;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import DAO.DAOUser;
import Views.Fenetre01;
import Views.FenetreHistoriqueCommande;
import Views.FenetreHomeUser;
import Views.FenetreHomeUsine;
import Views.FenetreLogin;
import Views.FenetreLot;
import Views.FenetreStock;
import model.User;

public class LoginController {

	FenetreLogin fenetreLogin;
	FenetreLot fenetreLot;
	FenetreStock fenetreStock;
	Fenetre01 fenetre01;
	DAOUser daou;
	User user;
	List<User> users;
	private Session session;
	
	public LoginController(FenetreLogin fenetreLogin,DAOUser daou,Session session) {
		super();
		this.session = session;
		this.fenetreLogin = fenetreLogin;
		this.daou = daou;
	
		fenetreLogin.getBtnConnexion().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String id = fenetreLogin.getTextFieldId().getText();
				String password = fenetreLogin.getPasswordField().getText();
				user = doConnexion(id,password);			
			}

		});	
	}
	
	private User doConnexion(String id, String password) {	
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		fenetreLogin.getLabelIncorrect().setText("Identifiant ou mot de passe incorrect");
		try {
			User user = daou.findUserByLogin(id);
			if (user.getUsername().equals(id)) {
				if(passwordEncoder.matches(password, user.getPassword())) {
					if("usine".equals(user.getRoleBean().getLibelle())) {
						FenetreHomeUsine fenetreHomeUsine = new FenetreHomeUsine(user);
						fenetreLogin.setVisible(false);
						fenetreHomeUsine.setVisible(true);
						Fenetre01 fenetre01 = new Fenetre01(user);
						FenetreStock fenetreStock = new FenetreStock();
						new HomeController(fenetre01,fenetreLot,fenetreHomeUsine,fenetreLogin,fenetreStock,session,user);
						return user;
					}
					if("client".equals(user.getRoleBean().getLibelle())) {
						FenetreHistoriqueCommande fenetreHistoriqueCommande = new FenetreHistoriqueCommande();
						FenetreHomeUser fenetreHomeUser= new FenetreHomeUser(user);
						fenetreLogin.setVisible(false);
						fenetreHomeUser.setVisible(true);
						Fenetre01 fenetre01 = new Fenetre01(user);
						new HomeController(fenetre01,fenetreLot,fenetreHomeUser,fenetreLogin,fenetreHistoriqueCommande,session,user);
						return user;
					}
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	public void init() {
		fenetreLogin.setVisible(true);	
	}
	
	
	
}

