package Controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.Commande;
import model.Medicament;
import model.Pharmacie;

public class MyDefaultTableModelCommande extends DefaultTableModel{
	
	List<Commande> commandes;
	private String columnNames [] = {"Numéro de la Commande", "Nom Pharmacie", "Nom du médicament", "Nom du Fournisseur", "Nombre de lots", "Prix Total (en €)"};
	HashSet<Commande> cModif;

	public MyDefaultTableModelCommande(List<Commande> commandes) {
		super();
		this.commandes = commandes;
		cModif = new HashSet<Commande>();
	}

	@Override
	public int getRowCount() {
		if (commandes == null) {
			return 0;
		} else {
		return commandes.size();
		}
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
		
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column > 0);
	}

	@Override
	public Object getValueAt(int row, int column) {
	
		Commande commande = commandes.get(row);
		Object value = null;
		
		switch (column) {
			case 0:
				value = commande.getId();
				break;
			case 1:
				value = commande.getPharmacie().getNomEntreprise();
				break;
			case 2:
				value = commande.getLot().getMedicament().getNom();
				break;
			case 3:
				value = commande.getLot().getUsine1().getNom();
				break;
			case 4:
				value = commande.getNbLot();
				break;
			case 5:
				value = commande.getLot().getMedicament().getPrix().floatValue() * commande.getLot().getQuantite();
				break;
			default:
				break;
		}
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {		
		
		Commande commande = commandes.get(row);
		Object value = null;
		
		switch (column) {

			case 1:
				commande.setId((int) aValue);
				break;
			case 2:
				commande.getPharmacie().setId((int) aValue);
				break;
			case 3:
				commande.getLot().setNum((int) aValue);
				break;
			case 4:
				commande.getLot().setQuantite((int) aValue);
				break; 
				
			default:
				break;		
		}
		
		cModif.add(commande);
		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		
		switch (columnIndex) {
			case 0:
				type = Integer.class;
				break;		
			case 1 :
				type = Integer.class;
				break;
			case 2 :
				type = Integer.class;
				break;
			case 3 :
				type = String.class;
				break;
			case 4 :
				type = Integer.class;
				break;
			case 5 :
				type = Float.class;
				break;
			
			default:
				break;
		}
		
		return type;
	}

	
}
