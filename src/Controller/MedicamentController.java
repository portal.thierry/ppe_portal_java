package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Session;

import DAO.DAOCommande;
import DAO.DAOLot;
import DAO.DAOMedicament;
import DAO.DAOPharmacie;
import DAO.DAOUsine;
import Views.Fenetre01;
import Views.FenetreAjoutMedicament;
import Views.FenetreHomeUser;
import Views.FenetreHomeUsine;
import Views.FenetreLot;
import model.Lot;
import model.Medicament;
import model.User;

public class MedicamentController {

	Fenetre01 fenetre01;
	FenetreAjoutMedicament fenetreAjoutMedicament;
	FenetreLot fenetreLot;
	FenetreHomeUser fenetreHomeUser;
	FenetreHomeUsine fenetreHomeUsine;
	DAOMedicament daom;
	DAOLot daol;
	DAOUsine daou;
	LotController lotController = null;
	Medicament medicament;
	List<Medicament> medicaments;
	List<Lot> lots;
	MyDefaultTableModelMedicament mDTMM;
	MyDefaultTableModelLot mDTML;
	int rows = 0;
	int columns = 0;
	private User user;
	private Session session;
	
	public MedicamentController(Fenetre01 fenetre01,FenetreAjoutMedicament fenetreAjoutMedicament,FenetreLot fenetreLot,FenetreHomeUser fenetreHomeUser,FenetreHomeUsine fenetreHomeUsine, DAOMedicament daom, DAOLot daol,Session session, User user) {
		super();
		this.fenetre01 = fenetre01;
		this.fenetreAjoutMedicament = fenetreAjoutMedicament;
		this.fenetreLot = fenetreLot;
		this.fenetreHomeUser = fenetreHomeUser;
		this.fenetreHomeUsine = fenetreHomeUsine;
		this.daom = daom;
		this.daol = daol;
		this.session = session;
		this.user = user;
		
		if ("usine".equals(user.getRoleBean().getLibelle())) {
			fenetre01.getBtnCancel().addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					doCancel();				
				}
		
			});
			fenetre01.getBtnDelete().addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					doDelete();				
				}

			});
			fenetre01.getBtnUpdate().addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					doUpdate();				
				}

			});	
			fenetre01.getBtnAjouter().addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					accederAjoutMedicament();
				}

			});
		
		}
		
		fenetre01.getBtnOk().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doSearch();				
			}

		});	
		
		fenetre01.getTable().addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
		      if (e.getClickCount() == 2) {
		          //Code pour gerer le double click	  
		    	  accederLotMedicament();
		    	  int idMedicament = Integer.parseInt(getIdCellSelected());
		    	  lots = getLotsForMedicament(idMedicament);
		    	  if (lotController == null) {
			    	  DAOUsine daou = new DAOUsine(session);
			    	  DAOPharmacie daop = new DAOPharmacie(session);
			    	  DAOCommande daoc = new DAOCommande(session);
		    		  lotController = new LotController(lots, fenetre01, fenetreLot, daol, daom, daou, daop, daoc, user, idMedicament);
		    	  }
		    	  else {
		    		  lotController.lots = getLotsForMedicament(idMedicament);
		    		  lotController.idMedicament = idMedicament;
		    	  }
		    	  
		        }
			}

		});	
		
		fenetreAjoutMedicament.getBtnAjouter().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doAdd();
				fenetreAjoutMedicament.setVisible(false);
				fenetre01.setVisible(true);
				doCancel();
			}

		});	
		
		fenetreAjoutMedicament.getBtnRetour().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doRetour();
			}

		});	
		
		fenetre01.getBtnRetour().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doRetourHome();
			}

		});
		
	}
	
	public void init() {
		medicaments = daom.findAll();
		mDTMM = new MyDefaultTableModelMedicament(medicaments);
		fenetre01.getTable().setModel(mDTMM);	
		fenetre01.setVisible(true);
	}
	
	private void accederAjoutMedicament() {
		int idMedicament = daom.getLatestIdMedicament()+1;
		String strIdMedicament = Integer.toString(idMedicament);
		fenetreAjoutMedicament.getTextField().setText(strIdMedicament);
		fenetre01.setVisible(false);
		fenetreAjoutMedicament.setVisible(true);
	}
	
	private void accederLotMedicament() {
		String id = getIdCellSelected();
		lots = daol.getLotsById(Integer.parseInt(id));
		mDTML = new MyDefaultTableModelLot(lots);
		fenetreLot.getTable().setModel(mDTML);
		fenetre01.setVisible(false);
		fenetreLot.setVisible(true);
	}
	
	private void doSearch() {
		medicaments = daom.findMedicamentsByName(fenetre01.getTextFieldRecherche().getText());
		mDTMM = new MyDefaultTableModelMedicament(medicaments);
		fenetre01.getTable().setModel(mDTMM);	
		fenetre01.setVisible(true);
	}
	
	private void doUpdate() {			
		for (Medicament medicament : mDTMM.cModif) {
			daom.update(medicament);
		}	
	}
	
	private void doCancel() {			
		init();		
	}
	
	private void doDelete() {	
		int index = fenetre01.getTable().getSelectedRow();		
		if(index != -1) {
			Medicament medicament = medicaments.get(index);
			daom.delete(medicament);
			medicaments.remove(index);
			mDTMM.fireTableDataChanged();	
		}
	}
	
	private void doAdd() {	

		int id = Integer.parseInt(fenetreAjoutMedicament.getTextField().getText());
		String nom = fenetreAjoutMedicament.getTextField_1().getText();
		BigDecimal prix = BigDecimal.valueOf((Double.valueOf(fenetreAjoutMedicament.getTextField_2().getText())));
		
		Medicament medicament = new Medicament(id,nom,prix);
		daom.save(medicament);
	}
	
	private void doRetour() {	
		fenetreAjoutMedicament.setVisible(false);
		fenetre01.setVisible(true);
	}
	
	private void doRetourHome() {	
		if ("usine".equals(user.getRoleBean().getLibelle())) {
			fenetre01.setVisible(false);
			fenetreHomeUsine.setVisible(true);
		}
		if ("client".equals(user.getRoleBean().getLibelle())) {
			fenetre01.setVisible(false);
			fenetreHomeUser.setVisible(true);
		}
	}
	
	private String getIdCellSelected() {			
		int rows = fenetre01.getTable().getSelectedRow();
		int columns = fenetre01.getTable().getSelectedColumn();
		int id = (int) fenetre01.getTable().getValueAt(rows, columns);
		return Integer.toString(id);
	}
	
	private void openAjout() {			
		fenetre01.setVisible(false);
		fenetreAjoutMedicament.setVisible(true);	
	}
	
	private List<Lot> getLotsForMedicament(int idMedicament) {
		lots = daol.getLotsById(idMedicament);
		return lots;
	}
	
}
