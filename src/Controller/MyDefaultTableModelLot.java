package Controller;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.Lot;

public class MyDefaultTableModelLot extends DefaultTableModel{
	
	List<Lot> lots;
	private String columnNames [] = {"Num", "Quantité", "Nom du Médicament","Usine de  Production"};
	HashSet<Lot> cModif;

	public MyDefaultTableModelLot(List<Lot> lots) {
		super();
		this.lots = lots;
		cModif = new HashSet<Lot>();
	}

	@Override
	public int getRowCount() {
		if (lots == null) {
			return 0;
		} else {
		return lots.size();
		}
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
		
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column > 3);
	}

	@Override
	public Object getValueAt(int row, int column) {
	
		Lot lot = lots.get(row);
		Object value = null;
		
		switch (column) {
			case 0:
				value = lot.getNum();
				break;
			case 1:
				value = lot.getQuantite();
				break;
			case 2:
				value = lot.getMedicament().getNom();
				break;
			case 3:
				value = lot.getUsine1().getNom();
				break;

			default:
				break;
		}
		
		return value;

	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {		
		
		Lot lot = lots.get(row);
		Object value = null;
		
		switch (column) {
			
			case 1:
				lot.setQuantite((int) aValue);
				break;
			case 2:
				lot.getMedicament().setNom((String) aValue);
				break;
			case 3:
				lot.getUsine1().setNom((String) aValue);
				break;
				
			default:
				break;		
		}
		
		cModif.add(lot);
		
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		
		switch (columnIndex) {
			case 1 :
				type = Integer.class;
				break;
			case 2 :
				type = Integer.class;
				break;
			case 3 :
				type = String.class;
				break;
			case 4 :
				type = String.class;
				break;
		
			default:
				type = String.class;
				break;
		}
		
		return type;
	}

	
}
