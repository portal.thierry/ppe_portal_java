package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.hibernate.Session;

import DAO.DAOCommande;
import DAO.DAOLot;
import DAO.DAOMedicament;
import Views.Fenetre01;
import Views.FenetreAjoutMedicament;
import Views.FenetreHistoriqueCommande;
import Views.FenetreHomeUser;
import Views.FenetreHomeUsine;
import Views.FenetreLogin;
import Views.FenetreLot;
import Views.FenetreStock;
import model.User;

public class HomeController {

	Fenetre01 fenetre01;
	FenetreLot fenetreLot;
	FenetreLogin fenetreLogin;
	FenetreHomeUsine fenetreHomeUsine;
	FenetreHomeUser fenetreHomeUser;
	FenetreStock fenetreStock;
	FenetreHistoriqueCommande fenetreHistoriqueCommande;
	private Session session;
	private User user;
	
	public HomeController(Fenetre01 fenetre01,FenetreLot fenetreLot,FenetreHomeUser fenetreHomeUser,FenetreLogin fenetreLogin,FenetreHistoriqueCommande fenetreHistoriqueCommande,Session session,User user) {
		super();
		this.fenetre01 = fenetre01;
		this.fenetreLot = fenetreLot;
		this.fenetreHomeUser = fenetreHomeUser;
		this.fenetreLogin = fenetreLogin;
		this.fenetreHistoriqueCommande = fenetreHistoriqueCommande;
		this.session = session;
		this.user = user;
		
		fenetreHomeUser.getBtnVoirLesMdicaments().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doAccederMedicaments();				
			}

		});	
		fenetreHomeUser.getBtnLogout().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				logout();				
			}

		});	
		fenetreHomeUser.getBtnHistorique().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				goHistorique();		
			}

		});	
	}
	
	public HomeController(Fenetre01 fenetre01,FenetreLot fenetreLot, FenetreHomeUsine fenetreHomeUsine,FenetreLogin fenetreLogin,FenetreStock fenetreStock, Session session, User user) {
		super();
		this.fenetre01 = fenetre01;
		this.fenetreLot = fenetreLot;
		this.fenetreHomeUsine = fenetreHomeUsine;
		this.fenetreLogin = fenetreLogin;
		this.fenetreStock = fenetreStock;
		this.session = session;
		this.user = user;
		
		fenetreHomeUsine.getBtnVoirLesMdicaments().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doAccederMedicaments();				
			}

		});	
		fenetreHomeUsine.getBtnStock().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("icii");
				goStock();		
			}

		});	
		fenetreHomeUsine.getBtnLogout().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				logout();				
			}

		});	
		
	}
	
	public void doAccederMedicaments() {	
		if (fenetreHomeUser != null) {
			fenetreHomeUser.setVisible(false);
		}
		if (fenetreHomeUsine != null) {
			fenetreHomeUsine.setVisible(false);
		}
		FenetreAjoutMedicament fAM = new FenetreAjoutMedicament();
		FenetreLot fL = new FenetreLot(user);
		DAOMedicament daom = new DAOMedicament(session);
		DAOLot daol = new DAOLot(session);
		
		MedicamentController medicamentController = new MedicamentController(fenetre01, fAM, fL, fenetreHomeUser, fenetreHomeUsine, daom, daol, session, user);
		medicamentController.init();
			
		fenetre01.setVisible(true);
	}
	
	private void logout() {
		if (fenetreHomeUser != null) {
			fenetreHomeUser.setVisible(false);
			fenetreLogin.setVisible(true);
			fenetreLogin.getTextFieldId().setText("");
			fenetreLogin.getPasswordField().setText("");
			fenetreLogin.getLabelIncorrect().setText("");
		}
		if (fenetreHomeUsine != null) {
			fenetreHomeUsine.setVisible(false);
			fenetreLogin.setVisible(true);
			fenetreLogin.getTextFieldId().setText("");
			fenetreLogin.getPasswordField().setText("");
			fenetreLogin.getLabelIncorrect().setText("");
		}
	}
	
	private void goHistorique() {
		DAOCommande daoc = new DAOCommande(session);
		CommandeController commandeController = new CommandeController(fenetreHistoriqueCommande,fenetreHomeUser, daoc, session, user);
		fenetreHomeUser.setVisible(false);
		fenetreHistoriqueCommande.setVisible(true);
		commandeController.init();
	}
	
	private void goStock() {
		DAOLot daol = new DAOLot(session);
		CommandeController commandeController = new CommandeController(fenetreStock,fenetreHomeUsine, daol, session, user);
		fenetreHomeUsine.setVisible(false);
		fenetreStock.setVisible(true);
		commandeController.init2();
	}
}
