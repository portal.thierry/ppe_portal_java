package Controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import model.Commande;
import model.Lot;
import model.Medicament;
import model.Pharmacie;

public class MyDefaultTableModelStock extends DefaultTableModel{
	
	List<Lot> lots;
	private String columnNames [] = {"Nom de l'usine", "Nom du médicament", "Nombre de lots", "Prix du médicament (en €)"};
	HashSet<Lot> cModif;

	public MyDefaultTableModelStock(List<Lot> lots) {
		super();
		this.lots = lots;
		cModif = new HashSet<Lot>();
	}

	@Override
	public int getRowCount() {
		if (lots == null) {
			return 0;
		} else {
		return lots.size();
		}
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
		
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column > 0);
	}

	@Override
	public Object getValueAt(int row, int column) {
	
		Lot lot = lots.get(row);
		Object value = null;
		
		switch (column) {
			case 0:
				value = lot.getUsine1().getNom();
				break;
			case 1:
				value = lot.getMedicament().getNom();
				break;
			case 2:
				value = lot.getQuantite();
				break;
			case 3:
				value = lot.getMedicament().getPrix();
				break;

			default:
				break;
		}
		
		return value;
		
		
	}

//	@Override
//	public void setValueAt(Object aValue, int row, int column) {		
//		
//		Lot lot = lots.get(row);
//		Object value = null;
//		
//		switch (column) {
//
//			case 1:
//				lot.getMedicament().setNom()((String) aValue);
//				break;
//			case 2:
//				lot.getPharmacie().setId((int) aValue);
//				break;
//			case 3:
//				lot.getLot().setNum((int) aValue);
//				break;
//
//				
//			default:
//				break;		
//		}
//		
//		cModif.add(lot);
//		
//	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> type = null;
		
		switch (columnIndex) {
			case 0:
				type = String.class;
				break;		
			case 1 :
				type = String.class;
				break;
			case 2 :
				type = Integer.class;
				break;
			case 3 :
				type = String.class;
				break;
			
			default:
				break;
		}
		
		return type;
	}

	
}
