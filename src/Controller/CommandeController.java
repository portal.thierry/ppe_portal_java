package Controller;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import org.hibernate.Session;

import DAO.DAOCommande;
import DAO.DAOLot;
import Views.FenetreHistoriqueCommande;
import Views.FenetreHomeUser;
import Views.FenetreHomeUsine;
import Views.FenetreStock;
import model.Commande;
import model.Lot;
import model.User;

public class CommandeController {

	FenetreHistoriqueCommande fenetreHistoriqueCommande;
	FenetreHomeUser fenetreHomeUser;
	FenetreStock fenetreStock;
	FenetreHomeUsine fenetreHomeUsine;
	DAOCommande daoc;
	DAOLot daol;
	MyDefaultTableModelCommande mDTMC;
	MyDefaultTableModelStock mDTMS;
	List<Commande> commandes;
	List<Lot> lots;
	private Session session;
	private User user;

	
	public CommandeController(FenetreHistoriqueCommande fenetreHistoriqueCommande ,FenetreHomeUser fenetreHomeUser ,DAOCommande daoc ,Session session ,User user) {
		super();
		this.fenetreHistoriqueCommande = fenetreHistoriqueCommande;
		this.fenetreHomeUser = fenetreHomeUser;
		this.daoc = daoc;
		this.session = session;
		this.user = user;

		fenetreHistoriqueCommande.getBtnRetour().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doRetour();				
			}

		});	
	}
		
		public CommandeController(FenetreStock fenetreStock ,FenetreHomeUsine fenetreHomeUsine ,DAOLot daol,Session session,User user) {
			super();
			this.fenetreStock = fenetreStock;
			this.fenetreHomeUsine = fenetreHomeUsine;
			this.daol = daol;
			this.session = session;
			this.user = user;
			
			fenetreStock.getBtnRetour().addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					doRetour2();				
				}

			});	
	}

	public void init() {
		commandes = daoc.getCommandesForPharmacie(user.getId());
		mDTMC = new MyDefaultTableModelCommande(commandes);
		fenetreHistoriqueCommande.getTable().setModel(mDTMC);	
		fenetreHistoriqueCommande.setVisible(true);
	}
	
	public void init2() {
		lots = daol.getLotsByUsine(user.getId());
		mDTMS = new MyDefaultTableModelStock(lots);
		fenetreStock.getTable().setModel(mDTMS);	
		fenetreStock.setVisible(true);
	}
	
	private void doRetour() {	
		fenetreHistoriqueCommande.setVisible(false);
		fenetreHomeUser.setVisible(true);
	}
	
	private void doRetour2() {
		fenetreStock.setVisible(false);
		fenetreHomeUsine.setVisible(true);
	}
}
