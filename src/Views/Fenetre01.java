package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.User;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Fenetre01 extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnUpdate;
	private JButton btnCancel;
	private JButton btnOk;
	private JButton btnDelete;
	private JTextField textField;
	private JLabel lblRecherche;
	private JButton btnAjouter;
	private JButton btnRetour;
	static User user;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Fenetre01 frame = new Fenetre01(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Fenetre01(User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1700, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnOk.setFont(new Font("Dialog", Font.BOLD, 24));
		btnOk.setBounds(1575, 15, 85, 40);
		contentPane.add(btnOk);
		
		if ("usine".equals(user.getRoleBean().getLibelle())) {
			btnUpdate = new JButton("Update");
			btnUpdate.setBounds(865, 720, 135, 40);
			btnUpdate.setFont(new Font("Dialog", Font.BOLD, 24));
			contentPane.add(btnUpdate);
			
			btnCancel = new JButton("Cancel");
			btnCancel.setBounds(600, 720, 135, 40);
			btnCancel.setFont(new Font("Dialog", Font.BOLD, 24));
			contentPane.add(btnCancel);
			
			btnDelete = new JButton("Delete");
			btnDelete.setBounds(735, 720, 135, 40);
			btnDelete.setFont(new Font("Dialog", Font.BOLD, 24));
			contentPane.add(btnDelete);
			
			btnAjouter = new JButton("Ajouter");
			btnAjouter.setBounds(200, 15, 135, 40);
			btnAjouter.setFont(new Font("Dialog", Font.BOLD, 24));
			contentPane.add(btnAjouter);
		}
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 100, 1625, 600);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		textField = new JTextField();
		textField.setFont(new Font("Dialog", Font.PLAIN, 24));
		textField.setBounds(1320, 15, 250, 40);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblRecherche = new JLabel("Recherche :");
		lblRecherche.setFont(new Font("Dialog", Font.BOLD, 24));
		lblRecherche.setBounds(1175, 15, 157, 40);
		contentPane.add(lblRecherche);
		
		btnRetour = new JButton("Retour");
		btnRetour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRetour.setFont(new Font("Dialog", Font.BOLD, 24));
		btnRetour.setBounds(35, 15, 135, 40);
		contentPane.add(btnRetour);
	}	
	public JButton getBtnUpdate() {
		return btnUpdate;
	}
	public JButton getBtnCancel() {
		return btnCancel;
	}
	public JButton getBtnOk() {
		return btnOk;
	}
	public JTable getTable() {
		return table;
	}
	public JButton getBtnDelete() {
		return btnDelete;
	}
	public JTextField getTextFieldRecherche() {
		return textField;
	}
	public JLabel getLblRecherche() {
		return lblRecherche;
	}
	public JButton getBtnAjouter() {
		return btnAjouter;
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}

}
