package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JPasswordField;

public class FenetreLogin extends JFrame {

	private JPanel textFieldMdp;
	private JTextField textFieldId;
	private JButton btnConnexion;
	private JPasswordField passwordField;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetreLogin frame = new FenetreLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FenetreLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1700, 800);
		textFieldMdp = new JPanel();
		textFieldMdp.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(textFieldMdp);
		textFieldMdp.setLayout(null);
		
		JLabel lblConnexion = new JLabel("Connexion");
		lblConnexion.setFont(new Font("Dialog", Font.BOLD, 90));
		lblConnexion.setBounds(599, 10, 516, 129);
		textFieldMdp.add(lblConnexion);
		
		textFieldId = new JTextField();
		textFieldId.setFont(new Font("Dialog", Font.PLAIN, 36));
		textFieldId.setBounds(599, 225, 485, 50);
		textFieldMdp.add(textFieldId);
		textFieldId.setColumns(10);
		
		JLabel lblIdentitfiant = new JLabel("Identitfiant");
		lblIdentitfiant.setFont(new Font("Dialog", Font.BOLD, 24));
		lblIdentitfiant.setBounds(599, 200, 150, 21);
		textFieldMdp.add(lblIdentitfiant);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe");
		lblMotDePasse.setFont(new Font("Dialog", Font.BOLD, 24));
		lblMotDePasse.setBounds(599, 323, 163, 27);
		textFieldMdp.add(lblMotDePasse);
		
		btnConnexion = new JButton("Connexion");
		btnConnexion.setFont(new Font("Dialog", Font.BOLD, 36));
		btnConnexion.setBounds(700, 475, 250, 70);
		textFieldMdp.add(btnConnexion);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Dialog", Font.PLAIN, 36));
		passwordField.setBounds(599, 350, 485, 50);
		textFieldMdp.add(passwordField);
		
		label = new JLabel("");
		label.setFont(new Font("Dialog", Font.BOLD, 24));
		label.setBounds(609, 412, 464, 51);
		textFieldMdp.add(label);
	}

	public JButton getBtnConnexion() {
		return btnConnexion;
	}
	public JTextField getTextFieldId() {
		return textFieldId;
	}
	public JPasswordField getPasswordField() {
		return passwordField;
	}
	public JLabel getLabelIncorrect() {
		return label;
	}
}
