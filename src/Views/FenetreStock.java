package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.User;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JButton;

public class FenetreStock extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JLabel lblHistoriqueCommande;
	private JButton btnRetour;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetreStock frame = new FenetreStock();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FenetreStock() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1701, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblHistoriqueCommande = new JLabel("Stock des médicaments de l'usine");
		lblHistoriqueCommande.setBounds(550, 10, 808, 67);
		lblHistoriqueCommande.setFont(new Font("Dialog", Font.BOLD, 48));
		contentPane.add(lblHistoriqueCommande);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(35, 115, 1625, 600);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		btnRetour = new JButton("Retour");
		btnRetour.setFont(new Font("Dialog", Font.BOLD, 24));
		btnRetour.setBounds(15, 10, 125, 40);
		contentPane.add(btnRetour);
	}
	public JTable getTable() {
		return table;
	}
	public JLabel getLblHistoriqueCommande() {
		return lblHistoriqueCommande;
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
}
