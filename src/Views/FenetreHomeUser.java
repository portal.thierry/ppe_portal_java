package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.User;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class FenetreHomeUser extends JFrame {

	private JPanel contentPane;
	static User user;
	private JButton btnVoirLesMdicaments;
	private JButton btnLogout;
	private JButton btnHistorique;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetreHomeUser frame = new FenetreHomeUser(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FenetreHomeUser(User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1700, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBienvenue = new JLabel("Bienvenue "+ user.getRoleBean().getLibelle() + " " + user.getUsername());
		lblBienvenue.setBounds(775, 0, 212, 24);
		contentPane.add(lblBienvenue);
		
		btnVoirLesMdicaments = new JButton("Voir les médicaments");
		btnVoirLesMdicaments.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnVoirLesMdicaments.setBounds(350, 100, 1000, 100);
		contentPane.add(btnVoirLesMdicaments);
		
		btnLogout = new JButton("Logout");
		btnLogout.setBounds(1550, 10, 100, 30);
		contentPane.add(btnLogout);
		
		btnHistorique = new JButton("Historique des commandes");
		btnHistorique.setBounds(350, 250, 1000, 100);
		contentPane.add(btnHistorique);
	}

	public JButton getBtnVoirLesMdicaments() {
		return btnVoirLesMdicaments;
	}
	public JButton getBtnLogout() {
		return btnLogout;
	}
	public JButton getBtnHistorique() {
		return btnHistorique;
	}
}
