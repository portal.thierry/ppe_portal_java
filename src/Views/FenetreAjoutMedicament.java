package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class FenetreAjoutMedicament extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldId;
	private JTextField textFieldNom;
	private JTextField textFieldPrix;
	private JLabel lblId;
	private JLabel lblNom;
	private JLabel lblPrix;
	private JLabel lblAjoutDunMedicament;
	private JButton btnAjouter;
	private JButton btnRetour;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetreAjoutMedicament frame = new FenetreAjoutMedicament();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FenetreAjoutMedicament() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1700, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblAjoutDunMedicament = new JLabel("Ajout d'un médicament");
		lblAjoutDunMedicament.setFont(new Font("Dialog", Font.BOLD, 48));
		lblAjoutDunMedicament.setBounds(533, 20, 573, 121);
		contentPane.add(lblAjoutDunMedicament);
		
		textFieldId = new JTextField();
		textFieldId.setFont(new Font("Dialog", Font.PLAIN, 24));
		textFieldId.setEditable(false);
		textFieldId.setBounds(630, 150, 430, 50);
		contentPane.add(textFieldId);
		textFieldId.setColumns(10);
		
		textFieldNom = new JTextField();
		textFieldNom.setFont(new Font("Dialog", Font.PLAIN, 24));
		textFieldNom.setBounds(630, 250, 430, 50);
		contentPane.add(textFieldNom);
		textFieldNom.setColumns(10);
		
		textFieldPrix = new JTextField();
		textFieldPrix.setFont(new Font("Dialog", Font.PLAIN, 24));
		textFieldPrix.setBounds(630, 350, 430, 50);
		contentPane.add(textFieldPrix);
		textFieldPrix.setColumns(10);
		
		lblId = new JLabel("Id :");
		lblId.setFont(new Font("Dialog", Font.BOLD, 24));
		lblId.setBounds(574, 150, 64, 50);
		contentPane.add(lblId);
		
		lblNom = new JLabel("Nom : ");
		lblNom.setFont(new Font("Dialog", Font.BOLD, 24));
		lblNom.setBounds(535, 250, 103, 50);
		contentPane.add(lblNom);
		
		lblPrix = new JLabel("Prix :");
		lblPrix.setFont(new Font("Dialog", Font.BOLD, 24));
		lblPrix.setBounds(548, 350, 64, 48);
		contentPane.add(lblPrix);
		
		btnAjouter = new JButton("Ajouter");
		btnAjouter.setFont(new Font("Dialog", Font.BOLD, 24));
		btnAjouter.setBounds(750, 500, 190, 50);
		contentPane.add(btnAjouter);
		
		btnRetour = new JButton("Retour");
		btnRetour.setFont(new Font("Dialog", Font.BOLD, 24));
		btnRetour.setBounds(15, 10, 190, 50);
		contentPane.add(btnRetour);
	}

	public JLabel getLblAjoutDunMdicament() {
		return lblAjoutDunMedicament;
	}
	public JTextField getTextField() {
		return textFieldId;
	}
	public JTextField getTextField_1() {
		return textFieldNom;
	}
	public JTextField getTextField_2() {
		return textFieldPrix;
	}
	public JLabel getLblId() {
		return lblId;
	}
	public JLabel getLblNom() {
		return lblNom;
	}
	public JLabel getLblNewLabel() {
		return lblPrix;
	}
	public JButton getBtnAjouter() {
		return btnAjouter;
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
}
