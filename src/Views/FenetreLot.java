package Views;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.User;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FenetreLot extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton btnRetour;
	private JButton btnAjouter;
	private JButton btnAcheter;
	private JTextField textFieldQtt;
	static User user;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FenetreLot frame = new FenetreLot(user);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FenetreLot(User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1700, 800);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(25, 100, 1650, 625);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		btnRetour = new JButton("Retour");
		btnRetour.setFont(new Font("Dialog", Font.BOLD, 24));
		btnRetour.setBounds(15, 10, 125, 40);
		contentPane.add(btnRetour);
		
		JLabel lblLotsDisponiblesPour = new JLabel("Lots disponibles pour un médicament");
		lblLotsDisponiblesPour.setFont(new Font("Dialog", Font.BOLD, 48));
		lblLotsDisponiblesPour.setBounds(357, 0, 931, 86);
		contentPane.add(lblLotsDisponiblesPour);
		
		if ("usine".equals(user.getRoleBean().getLibelle())) {
			btnAjouter = new JButton("Ajouter");
			btnAjouter.setBounds(25, 745, 125, 35);
			contentPane.add(btnAjouter);
		}
		
		if ("client".equals(user.getRoleBean().getLibelle())) {
			btnAcheter = new JButton("Acheter");
			btnAcheter.setBounds(25, 745, 125, 35);
			contentPane.add(btnAcheter);
		}
		
		textFieldQtt = new JTextField();
		textFieldQtt.setBounds(175, 745, 200, 25);
		contentPane.add(textFieldQtt);
		textFieldQtt.setColumns(10);
	
	}

	public JTable getTable() {
		return table;
	}
	public JButton getBtnRetour() {
		return btnRetour;
	}
	public JButton getBtnAjouter() {
		return btnAjouter;
	}
	public JButton getBtnAcheter() {
		return btnAcheter;
	}
	public JTextField getTextFieldQtt() {
		return textFieldQtt;
	}
}
