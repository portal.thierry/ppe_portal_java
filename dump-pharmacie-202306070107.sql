-- MariaDB dump 10.19  Distrib 10.5.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: pharmacie
-- ------------------------------------------------------
-- Server version	10.5.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `COMMANDE`
--

DROP TABLE IF EXISTS `COMMANDE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMMANDE` (
  `id` int(11) NOT NULL,
  `idPharmacie` int(11) DEFAULT NULL,
  `idLot` int(11) DEFAULT NULL,
  `nbLot` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `COMMANDE_ibfk_1` (`idLot`),
  KEY `COMMANDE` (`idPharmacie`),
  CONSTRAINT `COMMANDE` FOREIGN KEY (`idPharmacie`) REFERENCES `PHARMACIE` (`id`) ON DELETE CASCADE,
  CONSTRAINT `COMMANDE_ibfk_1` FOREIGN KEY (`idLot`) REFERENCES `LOT` (`num`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `COMMANDE`
--

LOCK TABLES `COMMANDE` WRITE;
/*!40000 ALTER TABLE `COMMANDE` DISABLE KEYS */;
INSERT INTO `COMMANDE` VALUES (1,72,53,104),(2,2,39,82),(3,42,60,280),(4,63,88,225),(5,7,92,64),(6,20,15,255),(7,20,39,267),(8,72,77,207),(9,52,97,252),(11,11,73,246),(12,6,88,235),(13,17,48,161),(14,25,26,228),(15,72,64,111),(16,48,4,144),(17,59,38,235),(18,38,39,149),(19,30,35,250),(20,11,8,55),(21,40,27,251),(22,57,75,203),(23,6,64,75),(24,52,9,286),(25,27,77,94),(26,13,96,202),(27,69,57,88),(28,11,12,189),(29,42,17,162),(30,64,34,82),(31,17,14,64),(32,61,56,153),(33,64,76,244),(34,70,95,62),(35,29,32,262),(36,23,45,248),(37,15,84,296),(38,17,87,69),(39,59,26,92),(40,45,86,139),(41,54,34,202),(42,20,60,153),(43,47,69,165),(44,74,40,56),(45,35,12,95),(46,49,57,148),(47,59,34,261),(48,1,20,231),(49,59,66,173),(50,50,23,108),(51,53,1,96),(52,57,90,163),(53,6,65,227),(54,33,2,203),(55,5,27,148),(56,48,26,294),(57,68,30,224),(58,75,33,104),(60,61,90,115),(61,74,93,201),(62,58,42,104),(63,5,71,97),(64,66,68,299),(65,60,10,282),(66,58,93,213),(67,14,54,52),(68,43,8,50),(69,15,31,144),(70,2,99,10),(71,2,54,785),(72,2,22,70),(73,2,30,36),(74,2,101,10),(75,2,96,100),(76,2,9,76),(77,2,100,11);
/*!40000 ALTER TABLE `COMMANDE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LOT`
--

DROP TABLE IF EXISTS `LOT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LOT` (
  `num` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  `typeMedicament` int(11) DEFAULT NULL,
  `usineDeProduction` int(11) DEFAULT NULL,
  PRIMARY KEY (`num`),
  KEY `usineDeProduction` (`usineDeProduction`),
  KEY `LOT_ibfk_3` (`typeMedicament`),
  CONSTRAINT `LOT_ibfk_3` FOREIGN KEY (`typeMedicament`) REFERENCES `MEDICAMENT` (`id`) ON DELETE CASCADE,
  CONSTRAINT `LOT_ibfk_4` FOREIGN KEY (`usineDeProduction`) REFERENCES `USINE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LOT`
--

LOCK TABLES `LOT` WRITE;
/*!40000 ALTER TABLE `LOT` DISABLE KEYS */;
INSERT INTO `LOT` VALUES (1,1800,12,5),(2,813,29,35),(3,4468,27,12),(4,1170,34,16),(5,1451,8,2),(6,4269,14,40),(7,2400,29,9),(8,1616,13,45),(9,700,6,44),(10,4820,14,34),(11,1378,33,37),(12,2607,27,19),(13,4822,8,9),(14,4928,21,13),(15,1237,22,33),(16,1650,7,5),(17,3816,3,32),(18,2254,26,9),(19,1679,33,39),(20,4928,17,23),(21,4450,9,20),(22,800,27,38),(23,4630,6,19),(24,3193,17,23),(25,1230,11,19),(26,3000,10,6),(27,3256,4,10),(28,2000,7,39),(29,3356,18,41),(30,2300,31,22),(31,4954,26,17),(32,1599,32,37),(33,2073,5,15),(34,2845,13,23),(35,897,31,12),(36,1849,30,19),(37,1470,27,13),(38,4584,18,33),(39,4676,4,10),(40,799,22,11),(41,4440,35,4),(42,2894,22,38),(43,874,13,37),(45,4974,35,33),(46,4596,19,27),(48,4300,9,23),(49,4032,11,43),(50,2901,32,6),(51,1892,31,12),(53,3934,29,42),(54,2000,8,10),(55,4389,5,18),(56,2094,24,28),(57,2081,13,42),(58,3004,21,41),(59,2596,16,27),(60,4599,28,4),(61,2263,27,25),(62,2400,11,19),(63,935,11,26),(64,1943,5,21),(65,2304,28,33),(66,4146,14,14),(67,2045,8,21),(68,1898,21,34),(69,3030,17,11),(70,209,6,36),(71,1903,32,30),(73,1428,14,16),(74,2289,30,15),(75,1431,17,11),(76,1910,14,15),(77,2060,31,29),(79,1128,27,34),(80,2688,21,29),(82,3463,21,10),(83,2376,11,37),(84,1634,20,4),(85,4447,22,45),(86,3756,14,41),(87,4246,26,43),(88,3068,11,46),(89,2546,12,18),(90,2609,26,14),(91,2685,7,38),(92,2751,25,12),(93,2010,24,49),(94,2961,28,41),(95,2252,21,25),(96,500,7,31),(97,1413,21,23),(98,3816,16,41),(99,10,10,1),(100,900,6,43),(101,1300,7,1),(102,1020,4,1),(103,7559,12,1),(104,1550,3,1),(105,1211,5,1),(107,125,6,1),(108,50,15,1),(109,1500,11,1),(110,2,8,1);
/*!40000 ALTER TABLE `LOT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MEDICAMENT`
--

DROP TABLE IF EXISTS `MEDICAMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MEDICAMENT` (
  `id` int(11) NOT NULL,
  `Nom` varchar(50) DEFAULT NULL,
  `Prix` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MEDICAMENT`
--

LOCK TABLES `MEDICAMENT` WRITE;
/*!40000 ALTER TABLE `MEDICAMENT` DISABLE KEYS */;
INSERT INTO `MEDICAMENT` VALUES (1,'Doliprane',17.99),(2,'CBD',57.99),(3,'Ili Hand Sanitizer',120.99),(4,'Amoxicillin and Clavulanate Potassium',50.99),(5,'Gabapentin',125.99),(6,'Omeprazole',100.86),(7,'Plan B',127.24),(8,'Thyrolar',150.73),(9,'Clindamycin Hydrochloride',170.40),(10,'Diltiazem Hydrochloride',35.34),(11,'Risperidone',73.73),(12,'Spironolactone',83.72),(13,'MicroMousse II Alcohol Free Hand Sanitizing Foam',107.75),(14,'Ultimate Daily Face Wash',92.62),(15,'Bion3',7.99),(16,'Med Aid Chest Rub',24.18),(17,'Nitroglycerin',7.52),(18,'Ammonia',126.44),(19,'Run Kang Alcohol Pads - MEDIUM',138.41),(20,'cool heat',115.45),(21,'KALI MURIATICUM',98.37),(22,'Crayola',159.24),(23,'Cephalexin',52.88),(24,'Anti-Bacterial Hand',152.21),(25,'Zolpidem Tartrate',86.39),(26,'Irinotecan Hydrochloride',185.42),(27,'Flu RELIEF',96.49),(28,'Ondansetron',152.55),(29,'smart sense pain relief',85.55),(30,'Conchae Argentum',25.10),(31,'Jock Itch',69.48),(32,'Carboplatin',187.21),(33,'ULTRA CONTROL',14.20),(34,'Treatment Set TS336851',11.06),(35,'Valium',37.39),(36,'Povidone-Iodine',141.78),(37,'ESSENTIAL SOLUTION HYALURONIC ACID MASK SHEET',106.34),(38,'BeKoool Itch Relief',29.55),(39,'childrens',97.71),(40,'Aquafresh',193.42),(41,'Dentifrice',104.24),(42,'Bupropion Hydrochloride',86.38),(43,'Lovastatin',79.65),(44,'Gum',14.50),(45,'COCOA BUTTER, PHENYLEPHRINE HYDROCHLORIDE',139.30),(46,'The Cure Sheer Cream',20.46),(47,'Helium',69.61),(48,'Nu Skin Nu Colour',77.18),(49,'Carbidopa and levodopa',66.28),(50,'Prednisone',10.17),(51,'childrens pain and fever',33.69),(52,'Panadol',165.14),(53,'Ketoconazole',146.07),(54,'LIME',117.72),(55,'FOSAMAX',10.09),(56,'Lamotrigine Extended Release',76.21),(57,'Vita',76.93),(58,'Naproxen Sodium',25.78),(59,'Imipramine Hydrochloride',157.71),(60,'hemorrhoidal',105.34),(61,'Foradil',112.46),(62,'C Lax Laxative',155.54),(63,'Sterile Water',35.15),(64,'Flu Relief Therapy Day Time',86.10),(65,'Dicyclomine Hydrochloride',128.88),(66,'NUMOTIZINE',68.99),(67,'Peptostrep Remedy',83.89),(68,'HOUSE DUST',127.09),(69,'AMARANTHUS SPINOSUS POLLEN',75.33),(70,'Laxative pills',133.36),(71,'Hydromorphone HCl',46.15),(72,'Naturals Pomegranate and Mango',176.35),(73,'RUE21 Posh Antibacterial Hand Sanitizer',171.20),(74,'G-Tuss-NL',35.85),(75,'Gabapentin',47.31),(76,'Allopurinol',155.53),(77,'Bentyl',103.04),(78,'Allergy Multi-Symptom Relief',146.69),(79,'all day relief',31.21),(80,'Metronidazole',166.55),(81,'Clindamycin hydrochloride',66.44),(82,'Lisinopril and Hydrochlorothiazide',124.85),(83,'Rx Act Gas Relief',131.57),(84,'Mentholatum Ironman',128.61),(85,'Oxybutynin Chloride',132.58),(86,'Captopril',73.67),(87,'Zithromax',96.29),(88,'Appeal Alcohol Foaming Hand Sanitizer',108.92),(89,'Clear Sinus and Ear',176.38),(90,'Hasol Revitalizing',172.73),(91,'Glimepiride',51.61),(92,'Mucus Relief Chest',149.14),(93,'Leader Pain Relief',175.75),(94,'Zicam',63.81),(95,'health mart ibuprofen',97.86),(96,'Smart Sense Hydrocortisone',131.05),(97,'Hydromorphone HCl',167.80),(98,'CITALOPRAM HYDROBROMIDE',7.80),(99,'Advil',139.20),(100,'LAMICTAL',145.30);
/*!40000 ALTER TABLE `MEDICAMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PHARMACIE`
--

DROP TABLE IF EXISTS `PHARMACIE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PHARMACIE` (
  `id` int(11) NOT NULL,
  `nomEntreprise` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PHARMACIE`
--

LOCK TABLES `PHARMACIE` WRITE;
/*!40000 ALTER TABLE `PHARMACIE` DISABLE KEYS */;
INSERT INTO `PHARMACIE` VALUES (1,'Donnelly Group','473 Comanche Hill','4614771116'),(2,'Rohan Inc','38 Magdeline Road','7475679017'),(3,'Metz Inc','68698 Ridgeview Trail','7708159994'),(4,'Berge-Ritchie','68503 Knutson Plaza','6624112603'),(5,'Deckow Inc','4972 Hovde Street','7889460203'),(6,'Greenholt-Heidenreich','51378 Tennyson Trail','4162585970'),(7,'Turcotte Inc','17191 Mifflin Point','7746743411'),(8,'Gleason-Littel','616 Algoma Crossing','7332960283'),(9,'Ebert Group','15712 Burrows Hill','4168534694'),(10,'Tremblay-McLaughlin','6 Carey Junction','2354464622'),(11,'Konopelski-Becker','890 Independence Park','1838073946'),(12,'Ritchie-Wiegand','93974 Maple Court','1647322019'),(13,'Daugherty, Kreiger and Casper','75 Melby Street','2767590336'),(14,'Bogisich-Reynolds','7190 Summer Ridge Plaza','7895201926'),(15,'Leffler-Olson','69616 Eliot Way','5138317869'),(16,'Klein Inc','8215 Rieder Plaza','2865162618'),(17,'Abshire-Ankunding','1 Buhler Trail','4853028692'),(18,'Ebert Inc','59769 Westend Avenue','1235969457'),(19,'Skiles-Wehner','2 Clemons Place','1607492617'),(20,'Jakubowski-Pfeffer','4431 John Wall Junction','3143505300'),(21,'Huels, O\'Hara and Schimmel','6 Cody Center','7977959270'),(22,'Harris, DuBuque and Morar','42308 New Castle Hill','8681718291'),(23,'Purdy, Hickle and Dickinson','90 Moulton Circle','2321132920'),(24,'Sauer, Sanford and Bergstrom','85 Manley Plaza','6011142170'),(25,'Jast LLC','4810 Cody Place','9145706112'),(26,'Beer-Conn','502 Independence Plaza','6009204058'),(27,'Volkman, Gerlach and Legros','29 Longview Way','7909808999'),(28,'Gulgowski and Sons','4 Johnson Drive','4086674621'),(29,'Mertz, Jenkins and Hane','9596 Dovetail Drive','6044389608'),(30,'Haag, Bode and Hamill','0 Hagan Center','9689807673'),(31,'Ferry, Collier and Huels','5 Hallows Place','9511135173'),(32,'Keebler, Runolfsdottir and Kshlerin','08 Lighthouse Bay Pass','1035649018'),(33,'Wilderman and Sons','68732 Milwaukee Junction','8632991883'),(34,'Toy Inc','7 Morning Point','8225414132'),(35,'Bashirian, Borer and Schumm','240 Debra Circle','4704741103'),(36,'Paucek, Connelly and Kessler','54780 Killdeer Way','9611442667'),(37,'Mueller, Walter and Grant','517 Blaine Point','6063187828'),(38,'Sauer and Sons','4572 Waywood Way','9846713531'),(39,'Schinner-Rempel','9 Moland Way','6953327293'),(40,'Dickinson-Hansen','15983 Sage Street','2297464681'),(41,'Sanford and Sons','80182 David Parkway','8279046911'),(42,'Cassin, Klocko and Ankunding','1 Buena Vista Crossing','1579783502'),(43,'Halvorson, Corwin and Kunze','3277 Mallory Alley','4604696797'),(44,'Tromp LLC','1788 Oneill Circle','8788478016'),(45,'Shanahan and Sons','23 Summerview Park','4786555134'),(46,'Mosciski, Lindgren and Cronin','1 Green Hill','4314445982'),(47,'Price-Beatty','28 Aberg Trail','6528706982'),(48,'Bechtelar LLC','8 Golden Leaf Hill','1483725400'),(49,'O\'Kon, Runolfsson and Kris','33999 Hazelcrest Point','1215714139'),(50,'Durgan LLC','14 Gerald Trail','1952631324'),(51,'McClure-Luettgen','5398 Duke Parkway','9502289127'),(52,'Goodwin and Sons','955 4th Place','9562087272'),(53,'Effertz and Sons','44590 Warrior Point','4789551960'),(54,'Considine-Heller','80 Rigney Alley','8671239171'),(55,'Erdman Group','0385 Raven Junction','3152290678'),(56,'Hilpert-Hessel','1934 Vidon Court','9003793602'),(57,'Conn and Sons','99657 Meadow Valley Alley','9025899765'),(58,'Lesch-Lehner','329 Buena Vista Trail','9687933393'),(59,'Vandervort, Kris and Treutel','90 Monterey Court','6784088654'),(60,'Gerlach-Bradtke','4 Monica Parkway','7445215015'),(61,'Anderson and Sons','32641 Straubel Court','2229962937'),(62,'Koch-Schinner','522 Monument Street','2971338314'),(63,'O\'Keefe, Heathcote and Medhurst','32 Carioca Crossing','5433279990'),(64,'Bergnaum, Leuschke and McLaughlin','3482 Buhler Plaza','8993516416'),(65,'Kris Inc','25 Eggendart Hill','5263539878'),(66,'Kessler-Quitzon','7008 Rigney Trail','1112768572'),(67,'Schmidt Inc','469 Vernon Pass','4722543110'),(68,'Beer, Hackett and Feeney','3 Hermina Trail','1659518995'),(69,'Predovic-Hudson','2978 Union Crossing','5549502485'),(70,'Marks-Hyatt','55 Fairfield Place','5839472911'),(71,'Reilly, Streich and Champlin','36452 Pleasure Alley','9741625801'),(72,'Reichel, Adams and Grady','95 Holmberg Plaza','4611086896'),(73,'Nitzsche-Turner','2 Prentice Way','5932628180'),(74,'Powlowski Group','7 Sycamore Terrace','2058761177'),(75,'Rath, Schamberger and Mayer','78563 Scoville Alley','4457620279');
/*!40000 ALTER TABLE `PHARMACIE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ROLE`
--

DROP TABLE IF EXISTS `ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ROLE` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL,
  `droits` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ROLE`
--

LOCK TABLES `ROLE` WRITE;
/*!40000 ALTER TABLE `ROLE` DISABLE KEYS */;
INSERT INTO `ROLE` VALUES (1,'client',69),(2,'usine',202);
/*!40000 ALTER TABLE `ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER`
--

DROP TABLE IF EXISTS `USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `USER_ibfk_1` (`role`),
  CONSTRAINT `USER_ibfk_1` FOREIGN KEY (`role`) REFERENCES `ROLE` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER`
--

LOCK TABLES `USER` WRITE;
/*!40000 ALTER TABLE `USER` DISABLE KEYS */;
INSERT INTO `USER` VALUES (1,'testUsine','$2a$10$GR8H.b3FxNKk7NbBiAo2v.LdPG6aRf47gREW4362x1D0F3suZPYdO',2),(2,'testClient','$2a$10$GR8H.b3FxNKk7NbBiAo2v.LdPG6aRf47gREW4362x1D0F3suZPYdO',1),(3,'cinstone2','f67f1762d52b885bb8a173caadc51b6097c02ec0dee3f13b609ff789e2d7a70c',2),(4,'cspong3','6152ec785bee773076bad096d9138d221fa85a08d551d74d88c45118824cab72',2),(5,'ispringthorpe4','ec08c0ff2c06e82c4cd3136015ef96ccd4e0c2cbb9396a5f489904461f42ce5a',1),(6,'cfellibrand5','78efe862ab99474fdac4c8efc61487103eb545e2ac972d3244d890bf8dcde37b',2),(7,'drapson6','af4b1a8ea8f188d97cd397e2fb6d1777b600c0833f526702493c57fec83ad739',2),(8,'tshelmerdine7','2bae1ed81adffa16661bf3f7af0440c651c89890870c244d6f3fd877b5a8297e',1),(9,'abrambill8','96e4c1b4da0ec4f4e8e37e5912f65d90006ad02ab52b47f091b733e0b6db7d02',1),(10,'mtyndall9','5bd67c6da18ad3a928dcd067e9e998fcd3acaf0055d30b9c4311a822ec7264ab',2),(11,'pjanninga','db3412f8080cf82e6a54d8797460159b0882564e09111999f2309d32a8c6d282',1),(12,'ebrindedb','39457f08aed17fa85e68fc5db6ffa707e6cb4fa9cf78e033acdafd13aa85f4ca',1),(13,'rivchenkoc','535a5e6426966ca7ef66ec6ac086d794600fdec8c5b350ee15de4495c7faa069',1),(14,'ngoosed','8690188bb25bdf8df629a0efa624352d59e8a9a290af34a673172b4322201adc',1),(15,'dfortoune','f6e1485ab87676d4373b6016ce35c8242e9785daab2da6c71e077ede33c59ed7',2),(16,'klangstrathf','604bfd46386483448bc64c075a6855b6f0f5bb94833fea05243a925d0d8b2148',2),(17,'ystortong','b962478b71fc1ebb2aa32317ae2c5927fb442e7491eb1cdf073671c517b81921',2),(18,'tolennikovh','fff68a6543796e58007b6492ff5b268209c580e8e0529223a48e7284de700861',2),(19,'jyurenini','3b1a25d8fc083957096665b5782f9f82ac1dad1e1dfd0069e5bde3c870969509',2),(20,'ilindhej','1ea2daac1c03b72c5f6e6339a7bb7f7123b47cc4782c8a9a3df54247b6a1e4bf',1),(21,'dflahertyk','7a66b7cd6a2f94687f8148659834d4319de0624fe408bcf15b59caafd6060d99',1),(22,'rlafflinal','ce8047d53f0e272130703be2073f03ce94c24de870fe13361ba88e20d27d6101',1),(23,'ssporem','6a8f018405e9a8f8eacbe79a4d11c54eb20c23122a3264a80b26c715bd80dc6e',2),(24,'bprattn','1e6ef12e65dd015a928d7f72aaecd472805985b122a692dd5e31e3a30d2b3da4',2),(25,'lgrindleo','77faea37c4d48c5f98ab2080ebe4554ac6502cf002c41735690aee8b40ff0ab2',2),(26,'wsearesp','fd2c84ed9cea37bb1cadc58a4fc9a3f91b7fb51303ed35216e403b18d32bd90a',2),(27,'budaleq','77dab39f121522c0bf6e586c784fed2b625efe85b87728d05ee2521e83584864',2),(28,'mkoppeckr','96708ca5ad84705dde1455e5e8afce5aa7027be499327fb228973dc2e7805ad8',2),(29,'ytrebbetts','6334beb95309c771d03ca69c27638d09d9382208f7fbc497bec6e67efa5fdc52',2),(30,'dcarinet','f66d28b747210e329cb35e46309b315d8b71f088ece36a084562b8707fcfc88d',2),(31,'alovartu','560f390bba2ec58098e08e5aad68f190c8604cf0984876e75f8c57f688125f4d',1),(32,'rcantov','0453b67296fe5e2e416b12b759a30722ec48cae4897f1b7e828ee0a3d653a81b',2),(33,'ecausonw','04a15f9cfb0f1627a2939ed8ba3ab8dbe80ff65ffe33cbd19786f59a98b4ce31',2),(34,'mmoorex','3cfd7a57ede3e8a7691e316b486724296e80a6d575be4fc4f3dcc3a21d1abd28',2),(35,'cjulliany','932f8e6bd72903863d603d502a0a663918db355d376c81c0ccdafe68374fac4d',2),(36,'pmccorkindalez','826db029a0217c10e207bdc82a8051cf46f4c699cd0aabfb7802e999918b6a43',1),(37,'aalekseev10','1f6cddda8cb9e75528b313270f2d0cf6178625f67ba95d98139d89608fdb57d2',2),(38,'ctouret11','08c7f688131ec6eda7f2eb3f548d4d0b7fc9c649cb854bf2448fccde0f54d7bb',1),(39,'aromushkin12','6168f0f49897025d59859492890a6a890e10db804f66735dbe6c867104501050',1),(40,'hgepheart13','fde37021996d6d00defde3c61a78214c7c72756d518efdfc356545c249649559',2),(41,'blummis14','726f110f884c896e23a64def9a430fe4127f27892ab93acfd91a4dff655dbbd0',1),(42,'vtumility15','a637449edb3469326a125e2f092e6ac557613f0a79eec16ed695a537c0102a9f',2),(43,'elondon16','749c85243b72dec0564088704ba3cc0365fc0544f64145dabe10edef155ce669',2),(44,'awille17','e6ac1d67e4c2912420d4a825dc49868d5673dfafc3080103add757ddbe6b58b5',2),(45,'lbasek18','861ee597e064eb2d0301833060f259584e0c6f85a8310c91513ffe07cd93086c',1),(46,'medgehill19','a024f930e50b19f3dee9dde2fab254c93dadb422f59040ad71ecef3cbbb41222',2),(47,'lkornas1a','a30cf5a94b3317ebe0a32045cf292d59c6b0eb30009410959d1714ac66e6e4a0',2),(48,'pburnel1b','c0c4c07f058bef116c2dc21c47df7229ba8734182469a9c356030307f751664c',1),(49,'rlottrington1c','d59b739d2665b8caf5925286297714478e9e1081c862712f431cc9c805082035',1),(50,'vcreber1d','026c4a340da741f785c8e5825406aa5ff0beb4dc50e9cbf3d9cdae59689388e7',1),(51,'gskillitt1e','b2c2fb52e6f7f05ff9318bf188118fab53fb6139ba4b23160bb67e42bc807d93',1),(52,'dzorn1f','8b390cbcf3e82b8d1ea7b8e2510eb07378f2707d6400873f78a2dee7a5658f84',2),(53,'tshobbrook1g','c134b771aa351d0c05828c2210f31b4f9b81abb9ac9f494cf0ed9e94ee39125c',2),(54,'babbe1h','0390caa76648dc26a8fe9d98be8b3edd1144de1eb41bf34ca1cf74d8ced05a41',2),(55,'asamarth1i','e92a3d8432c4cc63c86bbf82c7c5927145fcd0564ea43fe59f97ce123ddc8074',1),(56,'sfrostdicke1j','d0e789b69d9e1fa827f5f4a4a0aea5d9bf301355186e3e7a651d016345cfd766',1),(57,'aantonignetti1k','9a1875b3c7201a13a794908018e26e2e0240d40c22614a7aa2a5dac5ca7a5c7d',1),(58,'nfarfull1l','45b6e006e889f73b5290c6af048301f95cc0a2329b9fb6864cc921c03bdd981f',2),(59,'rmusgrave1m','60a1516ab09c4cdb7c6ba0892819cf23edd69f45bfd5e3314a31b2a1f7d62cdb',1),(60,'ejickles1n','2adae879a11df09cac589f791ab4ef888a821686731b2577c905de4145c59835',1),(61,'hechlin1o','0bdbbb1f6be63683202b91b951df5e5deee2c47cc7b7fb6a2dacd70bb5fbb9ab',2),(62,'hdominy1p','0f319c4aea98e6ccbdab14591727e76f7f4b52827a040d7f3c41098fee125fdb',1),(63,'jmajury1q','62f96155ce459a78f0b0f47b67ca5d6122080c0d1ca237e8fb7633b157c9399b',1),(64,'ymoncey1r','d4dd3dbd74253ac1dd19497e331d29359b4a679fac6fea0fccf255927142d578',2),(65,'ohoggetts1s','bcaa835d6e4beca270336690342b5551329dbf3b8521209131a1a3c05f3ea624',2),(66,'kfilimore1t','9f478845d1837256cd2d1b1c741d34c49b4659f81283ceb80f3b4ea381a67e20',2),(67,'deldredge1u','e76cfbfc59c93cb7dea4bf60092e0074bed62ea5e1cc0812da688cc234ab0569',2),(68,'wabrahamsohn1v','0251196de0a4a672875620d1841d67abc993de2f455d9bf2e6bd79e9ad93002c',1),(69,'kmatejic1w','db7dbd8642fce6bcdf0bc4c407afaa1764b34d4da8e404e0a92c6e2ebbabb02a',2),(70,'clovegrove1x','5284a28ec0fee42f31d2755f93e8315cf8084203754dffdf473a5cd1fdc8f3df',2),(71,'lickovits1y','c0146c1b5a7f57e5477f61a3c6d3dcf55149f76c99932f737bb02b3ff8157c3c',2),(72,'smcelree1z','deb3c57ed839e2ed0b6a6e7b3b26dba681254e6834034fa07ddcb0975fd41d19',1),(73,'afleckness20','d357f71c37c976a8f839010ff1c973a2e6163b4fcfef98cd5cae87220b4f3451',1),(74,'rreary21','3384d539d86e010174197108d437506c95baab749ada2c1850fe49e6464ebef4',2),(75,'amanna22','cab48e71b739e150a287ee485d7873af2a272822a4a47b86c5e16ab519e38463',2),(76,'osimonard23','607706577f1e8d3abb3ca683b7ff33a4af381392ce0f1ac6f065258d63bd76c6',1),(77,'mpendre24','b71c4907ed7f05a3fb76b89238cc7e0717c260e411f719ef1bb61eee096a2f7d',2),(78,'tgosselin25','df3d1c0ee721d38e7d682135edbfa41c2acfa856854e83f09e9475481514d3c8',1),(79,'chamberstone26','c427e4d29e58edf1f920268dca9dced119818778b595d93de1c220ea89118117',1),(80,'lyitzovitz27','2a75e5a60aa90a809e853e97995134a2f9e481b48811c8d9056ef416efaaa7d1',2),(81,'eisaacson28','c017c1826dd586192de4fbb1565778c030ed81c3eb22dd6ae63b2207584289fb',1),(82,'dservis29','7c98d74dcb02288f104bfe060dd211870af8244df1e83f4a055de07755bd499a',1),(83,'jholtum2a','97530dfc8e13f86975878cbd180318e9b19e74ff99fb5c8d8d3ed59fe34e9646',1),(84,'acannaway2b','ffbb6383e068840e14896726482dbc9484c687c6068cc218a07c92f340f5054e',1),(85,'apurves2c','14d3c6cf1511869763d14770740892ca35867e86180b8951bc81fb1f346fe4c8',2),(86,'mbeane2d','7d3c4ffc3d40338319d05ad38791cb5f69f16151f61a515f5c93c31a73abec83',1),(87,'ctrumpeter2e','6753de922523ea3b58da0246fe13ba77f15d17db284a6d96855b31d892fa65fa',2),(88,'ewheway2f','2e8126a9e96ad2f27666fa4e3d91022d2eec710c5c3cd6bf92cb7037cf3c1a96',2),(89,'gbrothwell2g','f0b3dda11ba5fac2185429708412273bdd2a612939b850edada8ed2c1b66465f',2),(90,'nhorwell2h','9d6d7668d5adf6cf92c63b5d6d6ea97ea362b53ba2dc7c86aecc21732bb8946d',2),(91,'sswancock2i','11de2bb1e4649fc44defe9e803567d6b816df7406f182cf1515ecc17544ddf47',1),(92,'pwallentin2j','ad12b26cc14203fa65415ef92353bf654f48a0f52d91438e70bc84412245bb24',1),(93,'tgilbert2k','c544224d2830ca1e90f7ed656827e551141bad3eb822742e18d8969ee73d14c0',2),(94,'cbeavis2l','50e2e901f017f862dafbd612853d6a3785e5b60acac993910ed541947e4e7014',1),(95,'klanfare2m','0c89cac0224b33dd440a9073b573ba800229523208cbf4665473b7928da1d9e8',2),(96,'lgallehock2n','742f48a7a09b374315e335ca4b9f5e2da6a80af9addd61e035253ec2cadfae3a',2),(97,'dheigl2o','b456cc9e3d6232f01fae2077fd54fe7bd7bbc94b2ce86b6cf7d31e126d9e33ed',2),(98,'ayashaev2p','ad771dd278f1d1d31db99691987f4957ebe3ed1043531640d7489c19ec29ac79',1),(99,'emelluish2q','48848dc5bb849ef6bf13c9b5e3320c0f832e8452d6be649b150ae77cdf64b1a9',1),(100,'pmearns2r','cc0ceb3073db3aacd20bfea4f1fdb7c50f2fa6a8a72f5a260a1bae58c5975a8e',2);
/*!40000 ALTER TABLE `USER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USINE`
--

DROP TABLE IF EXISTS `USINE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USINE` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `adresse` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `ville` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USINE`
--

LOCK TABLES `USINE` WRITE;
/*!40000 ALTER TABLE `USINE` DISABLE KEYS */;
INSERT INTO `USINE` VALUES (1,'Lind LLC','23 Cody Road','Binhailu'),(2,'Reynolds, Runolfsson and Brakus','14620 Ridgeview Lane','Qingquan'),(3,'Shields and Sons','8 Norway Maple Terrace','Masina'),(4,'Greenfelder and Sons','41100 Ronald Regan Crossing','Barcelona'),(5,'Greenfelder Inc','5 Warrior Lane','Decatur'),(6,'Dare Inc','4 Butterfield Lane','Donskoye'),(7,'Carter-Wilderman','17 Katie Park','London'),(8,'Shanahan, Hoppe and Cronin','64707 Kipling Point','Gongchenqiao'),(9,'Mraz-Gutkowski','929 Prentice Park','Vyzhnytsya'),(10,'Veum, Vandervort and McCullough','21480 Colorado Point','Issia'),(11,'Cruickshank Group','35629 Fulton Park','San Antonio'),(12,'Hettinger-Baumbach','0556 Prairie Rose Hill','Jawhar'),(13,'Jenkins Inc','56786 Burrows Road','Wabana'),(14,'Bogisich, Kulas and Keebler','4 Monica Terrace','Amancio'),(15,'Legros, Rodriguez and Rolfson','6275 Oakridge Lane','Shangmachang'),(16,'Effertz, Casper and Grady','580 Oxford Circle','Chengyue'),(17,'Harris Inc','503 Sauthoff Pass','Malvern'),(18,'O\'Keefe and Sons','0 Logan Alley','Nanfeng'),(19,'Dibbert, Koch and Lowe','0143 Rowland Junction','Wilanagara'),(20,'Stehr LLC','1645 Carioca Crossing','Esuk Oron'),(21,'Stoltenberg and Sons','809 Ludington Alley','Lisui'),(22,'Wuckert, Conn and Olson','11126 Hagan Parkway','Bunog'),(23,'Will LLC','859 Summer Ridge Place','Yerofey Pavlovich'),(24,'Drugs Industry','666 Hell Street','Lyon'),(25,'West-Hansen','1479 Ridgeway Street','Karangtengah'),(26,'Durgan Group','94 Butternut Place','Prado'),(27,'Ryan Group','5586 Wayridge Parkway','Manjacaze'),(28,'Bernier-Heaney','544 Esch Street','Quillota'),(29,'Greenfelder Group','42 Lawn Lane','Talisay'),(30,'Flatley Group','0 Miller Way','Ust’-Dzheguta'),(31,'Frami Group','292 Michigan Trail','Pancalan'),(32,'Altenwerth-MacGyver','66 Springview Avenue','Paris'),(33,'Schuppe-Murray','37307 Oak Valley Drive','Klau'),(34,'Nitzsche, Haley and Batz','146 Grayhawk Alley','El Bauga'),(35,'Lebsack-Cole','036 Saint Paul Junction','Berezovo'),(36,'Cormier LLC','4559 Fuller Trail','Doong'),(37,'Vandervort, Cummings and Farrell','70822 Kensington Avenue','Galovac'),(38,'Schuster, Bartell and Jacobs','886 8th Point','Haokou'),(39,'Stanton-Romaguera','270 Thierer Hill','Bosaso'),(40,'Steuber-Huels','6 Lighthouse Bay Drive','Topol’noye'),(41,'Parker and Sons','6 Steensland Lane','Wangchuanchang'),(42,'Braun-Kirlin','8431 Bay Drive','Ratíškovice'),(43,'Huel-Jast','6941 Fisk Place','Ajman'),(44,'Feeney, Deckow and Schneider','5 Spenser Alley','Paprotnia'),(45,'Bauch-O\'Keefe','05781 Mccormick Alley','Kalibunder'),(46,'Wolff and Sons','97 Fairview Road','Qianshan'),(47,'Johnston, Legros and Volkman','1202 Montana Point','Jianli'),(48,'Walter Inc','62943 Forest Run Terrace','Berlin'),(49,'MacGyver Group','58 Manley Street','Timezgadiouine'),(50,'Candy Land','10 Candy Street','CandyLand');
/*!40000 ALTER TABLE `USINE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'pharmacie'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-07  1:07:09
